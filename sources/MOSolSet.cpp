#include "MOSolSet.h"

//--------------------------------------------------
int MOSolSet::read(FILE *fichero){

    char linea[1024];
    unsigned int numPuntos, i;
    Point punto(this->dimension);

    rewind(fichero);
    numPuntos = 0;

    //while (!feof(fichero)){
	do{
            //fgets(linea, 1023, fichero);
            numPuntos++;
    } while (fgets(linea, 1023, fichero) != NULL);

    numPuntos--;
    rewind(fichero);

    for (i = 0; i < numPuntos; i++){
        punto.read(fichero);

        if (!(this->esDominado(punto)))
                this->add(punto, this);
    }

    return numPuntos;
}

//--------------------------------------------------
void MOSolSet::write(FILE *fichero){

    List<Point> *iterador;
    Point elemento(this->dimension);

    iterador = this;
    elemento = List<Point>::next(iterador);

    while (iterador != this){

            elemento.write(fichero);
            elemento = List<Point>::next(iterador);
    }
}

//--------------------------------------------------
void MOSolSet::add(Point &punto, List<Point> *posicion){

	// FOR N DIMENSIONAL PROBLEMS

    List<Point> *iterador;
    Point elemento(this->dimension);

    // check if there is a dominated element in the list
    iterador = this;
    elemento = List<Point>::next(iterador);

    while (iterador != this){
        if (1 == (punto.dominancia(elemento))){
                List<Point>::remove(iterador);
                elemento = List<Point>::get(iterador);
        }

        else
                elemento = List<Point>::next(iterador);
    }

    ((List<Point>*)this)->add(punto, this);
}

//--------------------------------------------------
bool MOSolSet::esDominado(Point &punto){

	// FOR N DIMENSIONAL PROBLEMS

    List<Point> *iterador;
    Point elemento(this->dimension);

    // check if there is a dominated element in the list
    iterador = this;
    elemento = List<Point>::next(iterador);

    while (iterador != this){

        if (1 == (elemento.dominancia(punto)))
                return true;

        elemento = List<Point>::next(iterador);
    }

    return false;
}

//--------------------------------------------------
unsigned int MOSolSet::dominados(MOSolSet &otraLista){

	// FOR N DIMENSIONAL PROBLEMS

    List<Point> *iterador;
    unsigned int numDominados;
    Point elemento (this->dimension);

    // get the number of dominated elements in 'otraLista'
    iterador = &otraLista;
    elemento = List<Point>::next(iterador);
    numDominados = 0;

    while (iterador != &otraLista){

        // add 1 if the current element is dominated
        if (1 == (this->esDominado(elemento))){

            numDominados++;

        }

        elemento = List<Point>::next(iterador);
    }

    return numDominados;
}

//--------------------------------------------------
float MOSolSet::metricaC(MOSolSet &otroPareto){

	// FOR N DIMENSIONAL PROBLEMS

    return (((float)this->dominados(otroPareto)) / ((float)otroPareto.size()));
}

//--------------------------------------------------
float MOSolSet::metricaBinariaEpsilon(MOSolSet &otroPareto, bool additive, bool minimizing) {
    double  eps, eps_k, eps_temp;
	
    double eps_j = 0;

    unsigned int i, j, k;

    // indicamos el numero de objetivos que tenemos
    unsigned int dim = 2;

    // pasamos los valores de la lista a vectores para acceder directamente a sus componentes

    // en a meteremos los valores del segundo pareto, primero el valor del obj0 y luego
    // del objetivo 1.En el caso I(A,B) sera B
    float a[(otroPareto.size()*2)];
    List<Point> *iterador;


    iterador = &otroPareto;
    elemento = List<Point>::next(iterador);
    i = 0;
    while (iterador != &otroPareto){
    	a[i++] = elemento.get(0);
    	a[i++] = elemento.get(1);
    	elemento = List<Point>::next(iterador);
    }

    // en b meteremos los valores del segundo pareto, primero el valor del obj0 y luego
    // del objetivo 1. En el caso I(A,B) sera A
    float b[(this->size()*2)];
    iterador = this;
	elemento = List<Point>::next(iterador);
	i = 0;
	while (iterador != this){
		b[i++] = elemento.get(0);
		b[i++] = elemento.get(1);
		elemento = List<Point>::next(iterador);
	}

	// ahora si calculamos la metrica

    if (additive) {
    	eps = DBL_MIN;   // DBL_MIN is the smallest double value, non-negative
	} else {
    	eps = 0;
	}

    for (i = 0; i < otroPareto.size(); i++) {
    	for (j = 0; j < this->size(); j++) {
            for (k = 0; k < dim; k++) {

                if (additive) {
                        if (minimizing)
                                eps_temp = b[j * dim + k] - a[i * dim + k];
                        else
                                eps_temp = a[i * dim + k] - b[j * dim + k];
                } else {
                        if( ((a[i * dim + k] < 0 && b[j * dim + k] > 0) ||
                            (a[i * dim + k] > 0 && b[j * dim + k] < 0) ||
                                a[i * dim + k] == 0 || b[j * dim + k] == 0) == true) {
                            fprintf(stderr, "Metric epsilon: Error in data file\n");
                            exit(1);
                        }
                        if (minimizing)
                            eps_temp = b[j * dim + k] / a[i * dim + k];
                        else
                            eps_temp = a[i * dim + k] / b[j * dim + k];
                }

                if (k == 0)
                    eps_k = eps_temp;
                else if (eps_k < eps_temp)
                    eps_k = eps_temp;
            }

            if (j == 0)
                eps_j = eps_k;
            else
                if (eps_j > eps_k)
                        eps_j = eps_k;
    	}

    	if (i == 0)
            eps = eps_j;
    	else if (eps < eps_j)
            eps = eps_j;
    }

    return eps;
}

//--------------------------------------------------
float MOSolSet::metricaM2(float sigma){

    List<Point> *iterador, *iterador2;
    Point elemento (this->dimension), elemento2(this->dimension);
    unsigned int auxiliar, sumatotal;
    float salida;

    iterador = this;
    elemento = List<Point>::next(iterador);
    sumatotal = 0;

    // this performance indicator will count the number of non dominated solutions of the Pareto front
    // with a distance higher than sigma
    while (iterador != this){

        iterador2 = this;
        elemento2 = List<Point>::next(iterador2);
        auxiliar = 0;

        while (iterador2 != this){

            if (elemento2.distanciaEuclidea(elemento) > sigma)
                auxiliar++;

            elemento2 = List<Point>::next(iterador2);
        }

        sumatotal += auxiliar;
        elemento = List<Point>::next(iterador);
    }

    // la salida final de la métrica será ese número de puntos tan o más alejados
    // que sigma dividido entre el número de puntos - 1

    if (this->size() > 1)
    	salida = ((float) sumatotal) / ((float) this->size() - 1);
    else
    	salida = ((float) sumatotal);

    return salida;

}

//--------------------------------------------------
float MOSolSet::metricaM3(){

    unsigned int i, dimensiones;
    Point elemento1, elemento2;
    List<Point> *iterador1, *iterador2;
    float sumatotal, maximadistancia, distancia;

    iterador1 = this;
    elemento1 = List<Point>::next(iterador1);
    dimensiones = elemento1.getDimension();
    sumatotal = 0.;

    // para esta métrica 3 tendremos que ver la máxima distancia entre todas
    // las componentes o dimensiones de los puntos del Pareto. Tendremos por tanto
    // tantas distancias máximas como dimensiones del Pareto. Habrá que devolver la raíz
    // cuadrada de esa suma de valores
    for (i = 0; i < dimensiones; i++){

        iterador1 = this;
        elemento1 = List<Point>::next(iterador1);
        maximadistancia = 0.;

        while (iterador1 != this){

            iterador2 = iterador1;
            elemento2 = List<Point>::next(iterador2);

            while (iterador2 != this){

                distancia = fabs(elemento1.get(i) - elemento2.get(i));

                if (maximadistancia < distancia){
                        maximadistancia = distancia;
                }

                elemento2 = List<Point>::next(iterador2);
            }

            elemento1 = List<Point>::next(iterador1);
        }

        sumatotal += (maximadistancia * maximadistancia);
    }

    return (sqrt(sumatotal));
}

//--------------------------------------------------
float MOSolSet::metricaS(){

    float volumen;
    // tenemos que ordenar los puntos del conjunto de menor a mayor según el valor de la
    // segunda coordenada.
    // Pasamos a una tabla hash (map) (clave el valor de y) y el valor de cada clave un puntero
    // al punto en cuestión
    // si hubiera dos elementos con igual coordenada 'y' se almacena el de mayor x
    map <float, Point *, less<float> > hash;

    List <Point> *iterador;
    Point *elemento;

    iterador = this;
    elemento = &(List<Point>::next(iterador));

    while (iterador != this){

        // insertamos en la tabla hash el punto actual. si ya existía un punto con esa coordenada 'y'
        // guardamos el que tenga mayor 'x'
        if(hash.find(elemento->get(1)) == hash.end() )
            hash[elemento->get(1)] = elemento;
        else
            if((hash[elemento->get(1)])->get(0) < elemento->get(0))
                hash[elemento->get(1)] = elemento;

        elemento = &(List<Point>::next(iterador));
    }

    // una vez que está en la tabla hash y se ha ordenado de menor a mayor
    // según la clave (coordenada y) vamos a ir sumando los volúmenes de cada punto
    // teniendo en cuenta que hay que restar las intersecciones de cada punto con su anterior
    map <float, Point *, less<float> >::iterator i,j;

    volumen = 0.;
    for(i = hash.begin(); i != hash.end(); i++){

        // añadimos el volumen  del iesimo elemento
        volumen += ((*i).second)->volumen();

        // restamos la intersección con el punto anterior
        if(i != hash.begin()){
            volumen -= ((*i).second)->volumenIntersec(*((*j).second));
            j ++;

        } else {
            j = hash.begin();
        }

    }

    return volumen;
}

//--------------------------------------------------
float MOSolSet::metricaS(Point &origen){

    // to implement this function we use a hash table (map) as a data structure. 
    // we ascending sort the points being the hash key the 'y' coordinate
    // the value of each key is a pointer to the corresponding point. 
    // If there are two points with equal 'y' we store the highest 'x'

    float volumen;
    map <float, Point *, less<float> > hash;

    List <Point> *iterador;
    Point *elemento;

    iterador = this;
    elemento = &(List<Point>::next(iterador));

    while (iterador != this){

        // insertamos en la tabla hash el punto actual. 
        // si ya existía un punto con esa coordenada 'y'
        // guardamos el que tenga mayor 'x'
        if(hash.find(elemento->get(1)) == hash.end() )
            hash[elemento->get(1)] = elemento;
        else
            if((hash[elemento->get(1)])->get(0) < elemento->get(0))
                hash[elemento->get(1)] = elemento;

        elemento = &(List<Point>::next(iterador));
    }

    // una vez que está en la tabla hash y se ha ordenado de menor a mayor
    // según la clave (coordenada y) vamos a ir sumando los volúmenes de cada punto
    // teniendo en cuenta que hay que restar las intersecciones de cada punto con su anterior
    map <float, Point *, less<float> >::iterator i,j;

    volumen = 0.;
    for(i = hash.begin(); i != hash.end(); i++){

        // añadimos el volumen  del iesimo elemento
        volumen += ((*i).second)->volumen(origen);

        // restamos la intersección con el punto anterior
        if(i != hash.begin()){
            volumen -= ((*i).second)->volumenIntersecMin(*((*j).second), origen);
            j ++;

        } else {
            j = hash.begin();
        }

    }

    return volumen;
}

//--------------------------------------------------
float MOSolSet::metricaM1(MOSolSet &paretoOptimo) {

    List<Point> *iterador, *iterador2;
    Point elemento, elemento2;
    float salida;
    double sumaTotal;
    double minDistance, currentDistance;

    iterador = this;
    elemento = List<Point>::next(iterador);
    sumaTotal = 0;

    // la métrica M1 devuelve la distancia entre el pareto y un pareto óptimo
    // pasado como argumento.
    // para obtener este valor vemos la mínima distancia euclídea de cada punto 
    // del pareto con todos los puntos del pareto óptimo acumulamos todos esos 
    // valores mínimos, uno por punto del pareto, y dividimos entre el 
    // número de puntos del Pareto para sacar la distancia media
    while (iterador != this){

        iterador2 = &paretoOptimo;
        elemento2 = List<Point>::next(iterador2);
        minDistance = elemento2.distanciaEuclidea(elemento);
        elemento2 = List<Point>::next(iterador2);

        while (iterador2 != &paretoOptimo){

            currentDistance = elemento2.distanciaEuclidea(elemento);

            if (currentDistance < minDistance)
                    minDistance = currentDistance;

            elemento2 = List<Point>::next(iterador2);
        }

        sumaTotal += minDistance;

        elemento = List<Point>::next(iterador);
    }

    salida = ((float) sumaTotal) / ((float) this->size());

    return salida;
}

//--------------------------------------------------
float MOSolSet::metricaER(MOSolSet &paretoOptimo) {

	// FOR N DIMENSIONAL PROBLEMS

    List<Point> *iterador, *iterador2;
    Point elemento, elemento2;
    float salida;
    bool existeEnOptimo;
    unsigned int contador;

    iterador = this;
    elemento = List<Point>::next(iterador);
    contador = 0;

    // ER metric returns a value between 0 and 1 resulted from the division of 
    // the number of Pareto solutions which are not in the optimal Pareto front
    while (iterador != this) {

        existeEnOptimo = false;

        iterador2 = &paretoOptimo;
        elemento2 = List<Point>::next(iterador2);

        while (iterador2 != &paretoOptimo && !existeEnOptimo){

        	if (elemento == elemento2)
        		existeEnOptimo = true;

            elemento2 = List<Point>::next(iterador2);
        }

        elemento = List<Point>::next(iterador);

        if (!existeEnOptimo)
        	contador++;
    }

    salida = ((float) contador) / ((float) this->size());

    return salida;
}

//--------------------------------------------------
void MOSolSet::metricaDistance2RefPoint (Point &refPoint, float lowerBounds[2], float upperBounds[2], float &avgResult, float &stdResult) {

    List<Point> *iterador;
    Point elemento, elemento2;

    float values [List<Point>::size()];

    iterador = this;
    elemento = List<Point>::next(iterador);

    // with this indicator we calculate the mean and standard deviation of the 
    // non dominated solutions with respect to the reference point

    unsigned int numElem = 0;
    while (iterador != this){

    	//  if point is outside the bounds it is not considered for the 
        //  performance indicator
    	if ((elemento.get(0) >= lowerBounds[0] && elemento.get(0) <= upperBounds[0]) 
                && (elemento.get(1) >= lowerBounds[1] 
                    && elemento.get(1) <= upperBounds[1])) {

			values[numElem] = elemento.distanciaEuclideaConPesos(refPoint, lowerBounds, upperBounds);

			//cout << values[numElem] << endl;

			numElem++;
    	} else {
    		cout << "Solution not considered because it is out of bounds " << elemento.get(0) << ", " << elemento.get(1) << endl;
    	}

        elemento = List<Point>::next(iterador);
    }

    // the final output of the performance indicator is the average and stdev of all the points
    avgResult = stdResult = 0.;
    for (unsigned int i = 0; i < numElem; i++)
    	avgResult += values[i];

    avgResult /= numElem;

    for (unsigned int i = 0; i < numElem; i++)
    	stdResult += (avgResult - values[i]) * (avgResult - values[i]);

    stdResult = sqrt (stdResult / (numElem - 1));

}

//--------------------------------------------------
double MOSolSet::polygonArea(vector <Point *> listOfPoints) {

   unsigned int i, j;
   double area = 0;

   for (i = 0; i < listOfPoints.size(); i++) {
	  j = (i + 1) % listOfPoints.size();
	  area += (listOfPoints[i])->get(0) * (listOfPoints[j])->get(1);
	  area -= (listOfPoints[i])->get(1) * (listOfPoints[j])->get(0);
   }

   area /= 2;
   return(area < 0 ? -area : area);

}

//--------------------------------------------------
float MOSolSet::metricaQuadrantAreas2RefPoint (Point &refPoint, float lowerBounds[2], float upperBounds[2]) {
    // this variable will control if the reference point is 
    // passed by Pareto front solutions
    bool optimistic = false;  

    // to implement this function we use a hash table (map) as a data structure. 
    // we ascending sort the points being the hash key the 'y' coordinate the 
    // value of each key is a pointer to the corresponding point. 
    // If there are two points with equal 'y' we store the highest 'x'

    map <float, Point *, less<float> > hash;
    List <Point> *iterador;
    Point *element;

    iterador = this;
    element = &(List<Point>::next(iterador));

    // find out what the bounds are for normalization. if reference point is lower than bounds, we set reference point as the limit for the bounds
    float lowerPoint[2];
    if (refPoint.get(0) < lowerBounds[0])
    	lowerPoint[0] = refPoint.get(0);
    else
    	lowerPoint[0] = lowerBounds[0];

    if (refPoint.get(1) < lowerBounds[1])
        	lowerPoint[1] = refPoint.get(1);
        else
        	lowerPoint[1] = lowerBounds[1];

    // normalization of the reference point
	refPoint.set(0,  (refPoint.get(0) - lowerPoint[0]) 
                        / (float)(upperBounds[0] - lowerPoint[0]));
	refPoint.set(1,  (refPoint.get(1) - lowerPoint[1]) 
                        / (float)(upperBounds[1] - lowerPoint[1]));

	//cout << "norm ref point " << refPoint.get(0) << "," << refPoint.get(1) << endl;

    while (iterador != this){

		// we normalize the values of the point
    	element->set(0, (element->get(0) - lowerPoint[0]) 
                        / (float)(upperBounds[0] - lowerPoint[0]));
    	element->set(1,  (element->get(1) - lowerPoint[1]) 
                        / (float)(upperBounds[1] - lowerPoint[1]));

		// insert the point in the hash table. if there was a point with this 'y' value, we store the highest 'x' value

    	//cout << "norm  point " << element->get(0) << "," << element->get(1) << endl;

		if(hash.find(element->get(1)) == hash.end() )
			hash[element->get(1)] = element;
		else
			if((hash[element->get(1)])->get(0) < element->get(0))
				hash[element->get(1)] = element;

        element = &(List<Point>::next(iterador));
    }

    // we have all the points in the hash table with ascending sort
    // according to key ('y' coordinate) (right to left in the Pareto front)
    map <float, Point *, less<float> >::iterator i;

    // arrays to store solutions according to the quadrant
    vector <Point*> quadrant1_3;
    vector <Point*> quadrant2;
    vector <Point*> quadrant4;

    for(i = hash.begin(); i != hash.end(); i++) {

        // check where the point is located (quadrant)
    	if ((*i).second->get(0) > refPoint.get(0)) {

    		if ((*i).second->get(1) > refPoint.get(1)) {
    			// 1st quadrant

    			//cout << "this solution belong to 1st quadrant: " << (*i).second->get(0) << "," << (*i).second->get(1) << endl;

    			optimistic = true;

    			// add the element to the list
    			Point *temp = new Point ((*i).second->get(0), (*i).second->get(1));
    			quadrant1_3.push_back(temp);

    		} else {
    			// 4th quadrant
    			//cout << "this solution belong to 4th quadrant: " << (*i).second->get(0) << "," << (*i).second->get(1) << endl;

				// add the element to the list
    			Point *temp = new Point ((*i).second->get(0), (*i).second->get(1));
				quadrant4.push_back(temp);
    		}

    	} else {

    		if ((*i).second->get(1) > refPoint.get(1)) {
    			// 2nd quadrant

    			//cout << "this solution belong to 2nd quadrant: " << (*i).second->get(0) << "," << (*i).second->get(1) << endl;

				// add the element to the list
    			Point *temp = new Point ((*i).second->get(0), (*i).second->get(1));
				quadrant2.push_back(temp);

    		} else {

    			// 3rd quadrant
    			//cout << "this solution belong to 3rd quadrant" << (*i).second->get(0) << "," << (*i).second->get(1) << endl;

    			optimistic = false;

    			// add the element to the list
    			Point *temp = new Point ((*i).second->get(0), (*i).second->get(1));
    			quadrant1_3.push_back(temp);
    		}

    	}

    }

	float area;		// a variable to store the total area of the 4 quadrants
    float area1_3, area2, area4;
    area = area1_3 = area2 = area4 = 0;

	// finally, for each list we add the last cut with the axis
	if (quadrant1_3.size() != 0) {

            // check if it is the 1st or 3rd quadrant and calculate
            if ( optimistic ) {
                // 1st quadrant

                unsigned int k;
                for (k = 0; k < quadrant1_3.size() - 1; k++ )
                        area1_3 += fabs(
                                (quadrant1_3[k]->get(0) - quadrant1_3[k + 1]->get(0)) 
                                    * (refPoint.get(1) - quadrant1_3[k]->get(1)));

                area1_3 += fabs(
                        (quadrant1_3[k]->get(0) - refPoint.get(0)) 
                            * (refPoint.get(1) - quadrant1_3[k]->get(1)));

            } else {
                // 3rd quadrant

                unsigned int k;
                for (k = quadrant1_3.size() - 1; k > 0; k-- ) {
                        area1_3 += fabs(
                                (quadrant1_3[k]->get(0) - quadrant1_3[k - 1]->get(0)) 
                                    * (refPoint.get(1) - quadrant1_3[k]->get(1)));
                }

                area1_3 += fabs(
                        (quadrant1_3[0]->get(0) - refPoint.get(0)) 
                            * (refPoint.get(1) - quadrant1_3[0]->get(1)));
            }
	}

	if (quadrant2.size() != 0) {
		// 2nd quadrant

		unsigned int k;
		for (k = quadrant2.size() - 1; k > 0; k-- )
                    area2 += fabs(
                            (quadrant2[k]->get(0) - quadrant2[k - 1]->get(0)) 
                                * (refPoint.get(1) - quadrant2[k]->get(1)));

		area2 += fabs(
                        (quadrant2[0]->get(0) - refPoint.get(0)) 
                        * (refPoint.get(1) - quadrant2[0]->get(1)));

	}

	if (quadrant4.size() != 0) {
		// 4rd quadrant

		unsigned int k;
		for (k = 0; k < quadrant4.size() - 1; k++ )
                    area4 += fabs(
                        (quadrant4[k]->get(0) - quadrant4[k + 1]->get(0)) 
                            * (refPoint.get(1) - quadrant4[k]->get(1)));

		area4 += fabs(
                        (quadrant4[k]->get(0) - refPoint.get(0)) 
                            * (refPoint.get(1) - quadrant4[k]->get(1)));

	}


	if (optimistic)
		area = area1_3 + area2 + area4;
	else
		area = area2 + area4 - area1_3;

    // remove memory from list of points
	unsigned int listSize = quadrant1_3.size();
	for (int i = (listSize - 1); i >= 0; i--)
		delete quadrant1_3[i];

	listSize = quadrant2.size();
	for (int i = (listSize - 1); i >= 0; i--)
		delete quadrant2[i];

	listSize = quadrant4.size();
	for (int i = (listSize - 1); i >= 0; i--)
		delete quadrant4[i];

    return area;

}
//--------------------------------------------------
Point* MOSolSet::getNadirPoint () {

	List<Point> *iterador;
	Point elemento (this->dimension);

	// get the number of dominated elements in 'otraLista'
	iterador = this;
	elemento = List<Point>::next(iterador);

	Point* nadir = new Point(this->dimension);

	nadir->setNadir(elemento);

	while (iterador != this){

		nadir->setNadir(elemento);

		elemento = List<Point>::next(iterador);
	}

	return nadir;
}



