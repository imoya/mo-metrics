#ifndef __POINT_H__
#define __POINT_H__

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>

// clase que implementa un punto en el espacio n-dimensional
class Point {

	// coordenadas del punto y la dimension de este
	float *coordenadas;
	unsigned int dimension;

public:

	// constructores y destructor
	Point(unsigned int dimension = 2);
	Point(float x, float y);
	Point(float x, float y, float z);
	Point(Point &unPunto);
	~Point();

	// funcion que devuelve la dimension del espacio del punto
	unsigned int getDimension();

	// funcion para asignar un valor a una de sus dimensiones
	float set(unsigned int dimension, float valor);

	// funcion para obtener el valor de una de sus dimensiones
	float get(unsigned int dimension);

	// funcion para leer los valores de un fichero
	unsigned int read(FILE *fichero);

	// funcion para escribir los valores en un fichero
	unsigned int write(FILE *fichero);

	// operador de asignacion
	Point & operator = (Point &otroPunto);

	// operador de igualdad
	bool operator == (Point &otroPunto);

	// funcion que indica la dominancia de un punto sobre otro
	// 1 si el punto es menor o igual que otroPunto en todas sus coordenadas
	// -1 si el punto es mayor en todas sus coordenadas
	// 0 si en algunas coordenadas es mayor y en otras menor
	// Esta función ser�a interesante redefinirla para el problema concreto
	int dominancia(Point &otroPunto);

	// funcion que devuelve la distancia euclídea con otro punto
	float distanciaEuclidea(Point &otroPunto);

	// devuelve el volumen del punto como multiplicación de sus coordenadas
	float volumen();

	// devuelve el volumen del punto como multiplicación de sus coordenadas teniendo en
	// cuenta un punto origen
	float volumen(Point &origen);

	// volumen de intersección entre dos puntos
	float volumenIntersec(Point &otroPunto);

	// volumen de intersección entre dos puntos teniendo en cuenta un origen de coordenadas
	// distinto al (0,0) y minimizando, es decir el punto de origen es mayor que los puntos del Pareto
	float volumenIntersecMin(Point &otroPunto, Point &origen);

	// volumen de intersección entre dos puntos teniendo en cuenta un origen de coordenadas
	// distinto al (0,0) y maximiznado, es decir el punto de origen es menor que los puntos del Pareto
	float volumenIntersecMax(Point &otroPunto, Point &origen);

	// Euclidean distance using weights and being normalised with lower and upper bounds
	float distanciaEuclideaConPesos(Point &refPoint, float lowerBounds[2], float upperBounds[2]);

	void setNadir(Point &otroPunto);

};

#endif
