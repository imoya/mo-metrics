#include "point.h"

//-------------------------------------------------
Point::Point(unsigned int dimension){

    unsigned int i;

    if (dimension < 1){
            fprintf(stderr, "Impossible to create a point with less than one dimension.\n");
            exit(1);
    }

    this->coordenadas = new float[dimension];
    this->dimension = dimension;

    // init object with more than 3 objectives
    for (i = 0; i < dimension; i++)
            this->coordenadas[i] = 0;
}

//-------------------------------------------------
Point::Point(float x, float y){

    this->dimension = 2;
    this->coordenadas = new float[2];
    this->coordenadas[0] = x;
    this->coordenadas[1] = y;
}

//-------------------------------------------------
Point::Point(float x, float y, float z){

    this->dimension = 3;
    this->coordenadas = new float[3];
    this->coordenadas[0] = x;
    this->coordenadas[1] = y;
    this->coordenadas[2] = z;
}

//-------------------------------------------------
Point::Point(Point &punto){

    unsigned int i;

    this->dimension = punto.dimension;
    this->coordenadas = new float[this->dimension];

    for (i = 0; i < this->dimension; i++)
            this->coordenadas[i] = punto.coordenadas[i];
}

//-------------------------------------------------
Point::~Point(){

    delete [] coordenadas;
}

//-------------------------------------------------
unsigned int Point::getDimension(){

    return (this->dimension);
}

//-------------------------------------------------
float Point::set(unsigned int dimension, float valor){

    return (this->coordenadas[dimension] = valor);
}

//-------------------------------------------------
float Point::get(unsigned int dimension){

    return (this->coordenadas[dimension]);
}

//-------------------------------------------------
unsigned int Point::read(FILE *fichero){

    unsigned int i;

    for (i = 0; i < this->dimension; i++){

            if (1 > (fscanf(fichero, "%f", this->coordenadas + i))){

                    fprintf(stderr, "Error reading a point from a file.\n");
                    exit(1);
            }
    }

    return (this->dimension);
}

//-------------------------------------------------
unsigned int Point::write(FILE *fichero){

    unsigned int i;

    fprintf(fichero, "%f", this->coordenadas[0]);

    for (i = 1; i < this->dimension; i++)
            fprintf(fichero, "\t%f", this->coordenadas[i]);

    fprintf(fichero, "\n");
    return (this->dimension);
}

//-------------------------------------------------
Point & Point::operator = (Point &otroPunto){

    unsigned int i;

    //Comprobar que tienen la misma dimension
    if (this->dimension != otroPunto.dimension){
            delete [] (this->coordenadas);
            this->coordenadas = new float[otroPunto.dimension];
            this->dimension = otroPunto.dimension;
    }

    for (i = 0; i < otroPunto.dimension; i++)
            this->coordenadas[i] = otroPunto.coordenadas[i];

    return (*this);
}

//-------------------------------------------------
bool Point::operator == (Point &otroPunto){

    unsigned int i;

    if (this->dimension != otroPunto.dimension)
            return false;

    for (i = 0; i < this->dimension; i++){

            if (this->coordenadas[i] != otroPunto.coordenadas[i])
                    return false;
    }

    return true;
}

//-------------------------------------------------
int Point::dominancia(Point &otroPunto){

	// FOR N DIMENSIONAL PROBLEMS

    unsigned int i;
    int dominancia;

    if (this->dimension != otroPunto.dimension){
            fprintf(stderr, "Error al calcular la dominancia de dos puntos con distinta dimension.\n");
            exit(1);
    }

    // if it is the same point, it is dominated and rejected
    if (*this == otroPunto)
            return 1;

    dominancia = 0;

    // go thru all the dimensions of the point to compare the values
    for (i = 0; i < this->dimension; i++){

            // if component ith is greater, it can dominate this
            if (this->coordenadas[i] > otroPunto.coordenadas[i]){

                    if (dominancia == 1)
                            return 0;

                    else if (dominancia == 0)
                            dominancia = -1;
            }

            // if it is lower in component ith, could be dominated
            else if (this->coordenadas[i] < otroPunto.coordenadas[i]){

                    // now, non-dominated
                    if (dominancia == -1)
                            return 0;

                    // up to know, it is dominated
                    else if (dominancia == 0)
                            dominancia = 1;
            }
    }

    return dominancia;
}

//--------------------------------------------------------------------
float Point::distanciaEuclidea(Point &otroPunto){

	// FOR N DIMENSIONAL PROBLEMS

    float auxiliar, suma;
    unsigned int i, dimensiones;

    if(this->dimension <= otroPunto.dimension)
        dimensiones = this->dimension;
    else 
        dimensiones = otroPunto.dimension;

    suma = 0.;

    for (i = 0; i < dimensiones; i++){

            auxiliar = otroPunto.coordenadas[i] - this->coordenadas[i];
            auxiliar = auxiliar * auxiliar;
            suma += auxiliar;
    }

    return (sqrt(suma));
}

//--------------------------------------------------------------------
float Point::volumen(){

	// FOR N DIMENSIONAL PROBLEMS

    float vol=1.;
    
    for(unsigned int i=0; i<this->dimension; i++)
        vol *= this->coordenadas[i];
    
    return vol;
    
}

//--------------------------------------------------------------------
float Point::volumen(Point &origen){

	// FOR N DIMENSIONAL PROBLEMS

    float vol = 1.;
    
    if(origen.dimension != this->dimension){
        fprintf(stderr, "Error while calculating the volume of a point with respect to origin.\n");
        exit(1);
    }
    
    for(unsigned int i=0; i<this->dimension; i++)
        vol *= fabs(this->coordenadas[i] - origen.coordenadas[i]);
    
    return vol;
    
}

//--------------------------------------------------------------------
float Point::volumenIntersec(Point &otroPunto){

	// FOR N DIMENSIONAL PROBLEMS

    Point puntoVolInt(this->dimension);
    
    for(unsigned int i=0; i<this->dimension; i++){
        if(this->coordenadas[i] <= otroPunto.coordenadas[i])
            puntoVolInt.set(i, this->coordenadas[i]);
        else
            puntoVolInt.set(i, otroPunto.coordenadas[i]);
    }
        
    return puntoVolInt.volumen();    
}

//--------------------------------------------------------------------
float Point::volumenIntersecMin(Point &otroPunto, Point &origen){

	// FOR N DIMENSIONAL PROBLEMS

    Point puntoVolInt(this->dimension);
    
    for(unsigned int i=0; i<this->dimension; i++){
        if(this->coordenadas[i] > otroPunto.coordenadas[i])
            puntoVolInt.set(i, this->coordenadas[i]);
        else
            puntoVolInt.set(i, otroPunto.coordenadas[i]);
    }
       
    return puntoVolInt.volumen(origen);    
}

//--------------------------------------------------------------------
float Point::volumenIntersecMax(Point &otroPunto, Point &origen){

	// FOR N DIMENSIONAL PROBLEMS

    Point puntoVolInt(this->dimension);

    for(unsigned int i=0; i<this->dimension; i++){
        if(this->coordenadas[i] < otroPunto.coordenadas[i])
            puntoVolInt.set(i, this->coordenadas[i]);
        else
            puntoVolInt.set(i, otroPunto.coordenadas[i]);
    }

    return puntoVolInt.volumen(origen);
}

//--------------------------------------------------------------------
float Point::distanciaEuclideaConPesos(Point &refPoint, float lowerBounds[2], float upperBounds[2]) {

    float weights[2];

    // till now, fixed to 0.5
    weights[0] = weights[1] = 0.5;

    float auxiliar, suma;
    unsigned int i, dimensiones;

    if(this->dimension <= refPoint.dimension)
        dimensiones = this->dimension;
    else
        dimensiones = refPoint.dimension;

    suma = 0.;

    for (i = 0; i < dimensiones; i++){

		auxiliar = (this->coordenadas[i] - refPoint.coordenadas[i]) / (upperBounds[i] - lowerBounds[i]);
		auxiliar = auxiliar * auxiliar;
		suma += (weights[i] * auxiliar);
    }

    return (sqrt(suma));

}
//--------------------------------------------------------------------
void Point::setNadir(Point &otroPunto) {

	for (unsigned int i=0; i<this->dimension; i++) {
		this->coordenadas[i] = fmax(this->coordenadas[i], otroPunto.get(i));
	}
}


