#ifndef __LIST_H__
#define __LIST_H__

#include <stdio.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <time.h>
#include <string.h>
#include <malloc.h>

//Definicion de la clase Lista de elementos de la clase Tipo
template<class Tipo> class List{

protected:
	//Es una lista doble y tendra punteros al anterior y siguiente elemento
	List *anterior;
	Tipo elemento;
	List *siguiente;

public:
	//Constructor y Destructor
	List<Tipo>();
	List<Tipo>(List<Tipo> &unaLista);
	~List();

	//Funcion para añadir un elemento en una posicion
	static Tipo & add(Tipo &elemento, List<Tipo> *posicion);

	//Funcion para eliminar el elemento de una posicion
	static void remove(List<Tipo> *&posicion);

	//Funcion para obtener el elemento de una posicion
	static Tipo & get(List<Tipo> *posicion);

	//Funcion para obtener el siguiente elemento al de la posicion
	//posicion se modificara para apuntar al siguiente elemento al que apuntaba
	static Tipo & next(List<Tipo> *&posicion);

	//Funcion para obtener el elemento anterior al de la posicion
	//posicion se modificara para apuntar al elemento anterior al que apuntaba
	static Tipo & prev(List<Tipo> *&posicion);

	//Funcion que devuelve el numero de elementos de la lista
	unsigned int size();

	//Funcion que añade un elemento de otro conjunto a this sacandolo del conjunto
	//al que pertenecia (Siempre que sea un elemento valido "con solucion")
	static void bring(List<Tipo> *posicionInsertar, List<Tipo> *elemento);

	//Funcion que copia la lista en la otra
	List<Tipo> & operator = (const List<Tipo> &unaLista);

	//Funcion que vacia la lista
	void vacia();
};

//---------------------------------------------------------------
template<class Tipo> List<Tipo>::List(){

	this->anterior = this->siguiente = this;
}

//---------------------------------------------------------------
template<class Tipo> List<Tipo>::List(List<Tipo> &unaLista){

	List<Tipo> *iterador, *cabeza;

	this->anterior = this->siguiente = this;

	cabeza = &unaLista;
	iterador = unaLista.siguiente;

	while (iterador != cabeza){

		this->add(iterador->elemento, this);
		iterador = iterador->siguiente;
	}
}

//---------------------------------------------------------------
template<class Tipo> List<Tipo>::~List<Tipo>(){

	List<Tipo> *iterador, *siguiente, *cabeza;

	//Eliminar todos los elementos de la lista
	cabeza = iterador = this;
	iterador = iterador->siguiente;

	while (iterador != cabeza){

		siguiente = iterador->siguiente;

		//Antes de llamar a delete(iterador) asegurarse de que se
		//saca de la lista.
		iterador->siguiente->anterior = iterador->anterior;
		iterador->anterior->siguiente = iterador->siguiente;
		iterador->siguiente = iterador->anterior = iterador;
		delete (iterador);

		//Pasar al siguiente elemento
		iterador = siguiente;
	}
}

//---------------------------------------------------------------
template<class Tipo> Tipo & List<Tipo>::add(Tipo &elemento, List<Tipo> *posicion){

	List<Tipo> *nuevoElemento;

	//Crear y asignar el nuevo elemento
	nuevoElemento = new List<Tipo>;
	nuevoElemento->elemento = elemento;

	//Insertarlo en la lista en la posicion deseada
	nuevoElemento->anterior = posicion->anterior;
	nuevoElemento->siguiente = posicion;
	posicion->anterior->siguiente = nuevoElemento;
	posicion->anterior = nuevoElemento;
	
	return elemento;
}

//---------------------------------------------------------------
template<class Tipo> void List<Tipo>::remove(List<Tipo> *&posicion){

	List<Tipo> *siguiente;

	//Sacar el elemento de la lista
	siguiente = posicion->siguiente;
	posicion->anterior->siguiente = posicion->siguiente;
	posicion->siguiente->anterior = posicion->anterior;
	posicion->siguiente = posicion->anterior = posicion;

	//eliminarlo
	delete (posicion);
	posicion = siguiente;
}

//---------------------------------------------------------------
template<class Tipo> Tipo & List<Tipo>::get(List<Tipo> *posicion){

	return (posicion->elemento);
}

//---------------------------------------------------------------
template<class Tipo> Tipo & List<Tipo>::next(List<Tipo> *&posicion){

	posicion = posicion->siguiente;
	return (posicion->elemento);
}

//---------------------------------------------------------------
template<class Tipo> Tipo & List<Tipo>::prev(List<Tipo> *&posicion){

	posicion = posicion->anterior;
	return (posicion->elemento);
}

//---------------------------------------------------------------
template<class Tipo> unsigned int List<Tipo>::size(){

	List<Tipo> *iterador;
	unsigned int numElementos;

	//Darle una vuelta a la lista contabilizando los elementos
	iterador = this->siguiente;
	numElementos = 0;

	while (iterador != this){

		iterador = iterador->siguiente;
		numElementos++;
	}

	return (numElementos);
}

//---------------------------------------------------------------
template<class Tipo>
void List<Tipo>::bring(List<Tipo> *posicionInsertar, List<Tipo> *elemento){

	if (posicionInsertar == elemento)
		return;

	elemento->siguiente->anterior = elemento->anterior;
	elemento->anterior->siguiente = elemento->siguiente;

	elemento->anterior = posicionInsertar->anterior;
	elemento->siguiente = posicionInsertar;
	posicionInsertar->anterior->siguiente = elemento;
	posicionInsertar->anterior = elemento;
}

//-----------------------------------------------------------------
template<class Tipo>
void List<Tipo>::vacia(){

	List<Tipo> *iterador;

	iterador = this->siguiente;

	while (iterador != this){
		List<Tipo>::remove(iterador);
	}
}

//-----------------------------------------------------------------
template<class Tipo>
List<Tipo> & List<Tipo>::operator = (const List<Tipo> &unaLista){

	List<Tipo> *iterador;

	this->vacia();
	iterador = unaLista.siguiente;

	while (iterador != &unaLista){

		List<Tipo>::add(iterador->elemento, this);
		iterador = iterador->siguiente;
	}

	return (*this);
}

#endif
