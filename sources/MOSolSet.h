#ifndef __MOSOLSET_H__
#define __MOSOLSET_H__

#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>

#include <map>
#include <iostream>
#include <vector>

#include "list.h"
#include "point.h"

using namespace std;

/**
 * Clase que implementa un conjunto de soluciones para un problema multiobjetivo
 */
class MOSolSet : public List<Point> {

	unsigned int dimension;

public:

	/**
	 * constructor y destructor
	 */
    MOSolSet(unsigned int _dimension) { this->dimension = _dimension; };
    ~MOSolSet() {};

    /**
     * funcion que lee puntos de un fichero y los mete en el conjunto.
     * devuelve el numero de puntos que contiene el fichero
     */
    int read(FILE *fichero);

    /**
     * funcion que escribe los puntos en un fichero
     */
    void write(FILE *fichero);

    /**
     * funcion que aade un punto a la lista pero eliminando los que son dominados
     */
    void add(Point &punto, List<Point> *posicion);

    /**
     * funcion que indica si un punto es dominado o no por algun elemento de la lista
     * El punto tendra dimension == 2
     */
    bool esDominado(Point &punto);

    /**
     *  funcion que devuelve el numero de elementos en otra lista que son dominados por esta
     */
    unsigned int dominados(MOSolSet &otraLista);

    /**
     * funcion que devuelve la medida C(this, otroPareto)
     * C: con esta metrica comparamos dos paretos. Nos mide el grado en el que un Pareto cubre al otro
     * si C(Y`,Y``) = 1 es porque todas las soluciones de Y` dominan a las de Y``.
     * Sera 0 en el caso de que no dominen ninguna.
     * No es commutativa, por lo que habra que calcular la metrica para (Y`, Y``) y (Y``,Y`)
     */
    float metricaC(MOSolSet &otroPareto);

    /**
     * IMPORTANT:
     *   In the case of minimization the value -epsilon (for the
 	 *   additive case) resp. 1/epsilon (for the multiplicative version) is
 	 *   considered and returned. Example: suppose f1 is to be minimized and
 	 *   f2 to be maximized, and the multiplicative epsilon value computed by
 	 *   this program is 3.0; this means that the considered nondominated front
 	 *   needs to be multiplied by 1/3 for all f1 values and by 3 for all
 	 *   f2 values. Thus, independently of which type of problem one considers
 	 *   (minimization, maximization, mixed minimization/maximization), a lower
 	 *   indicator value corresponds to a better approximation set.
 	 *   @return el valor epsilon
 	 *   @param otroPareto es el frente de Pareto con el que comparar
 	 *   @param additive un valor booleano que si es verdadero nos indica que obtenemos la metrica aditiva
 	 *   		y si es negativo la multiplicativa
 	 *   @param minimizing true si estamos minimizando, false si maximizamos
     */
    float metricaBinariaEpsilon(MOSolSet &otroPareto, bool additive, bool minimizing);

    /**
     * funcion que devuelve la métrica M2(this, sigma)
     * M2: nos mide la distribución de un Pareto teniendo en cuenta el valor sigma dado
     * ese valor sigma nos da el radio de las partes en las que se distribuye el Pareto
     */
    float metricaM2(float sigma);

    /**
     * función que devuevle la métrica M1 con respecto a otro pareto óptimo, en teoría
     * M1: media de la distancia euclídea entre el pareto y un pareto óptimo pasado como argumento
     */
    float metricaM1(MOSolSet &paretoOptimo);

    /**
     * función que devuelve la métrica M3(this)
     * M3: extensión de un Pareto viendo la distancia entre los extremos del mismo
     */
    float metricaM3();

    /**
     * función que devuelve la métrica S. Los puntos tendrán que tener 2 dimensiones
     * si intentamos minimizar no se toma el punto de origen (0,0) por lo que usaremos la 2ª variatne
     * de la función. Para maximizar y utilizar el punto de origen (0,0) usaremos metricaS()
     */
    float metricaS();
    float metricaS(Point &origen);

    /**
     * función que devuelve la métrica ER entre el conjunto de la clase y el pasado como
     * argumento, que tiene que ser el pareto óptimo o pseudo-optimo
     */
    float metricaER(MOSolSet &paretoOptimo);

    /**
	* function to calculate the weighted Euclidean distance of the nondominated solutions of the Pareto front with respect to the
	* reference point
	*/
    void metricaDistance2RefPoint(Point &refPoint, float lowerBounds[2], float upperBounds[2], float &avgResult, float &stdResult);

    /**
	* function to calculate the area of a polygon defined by N points. The last point is assumed to be the same as the first one. Courtesy of Paul Bourke,
	* http://local.wasp.uwa.edu.au/~pbourke/geometry/polyarea/
	*/
	double polygonArea(vector <Point *> listOfPoints);

    /**
	* function to calculate coverage taking the reference point as origin. only solutions of the first quadrant are considered to generate the area
	*/
    float metricaQuadrantAreas2RefPoint (Point &refPoint, float lowerBounds[2], float upperBounds[2]);

    Point* getNadirPoint();
};

#endif
