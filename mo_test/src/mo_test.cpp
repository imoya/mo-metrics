//============================================================================
// Name        : mo_test.cpp
// Author      : Ignacio Moya
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "MOSolSet.h"
#include "list.h"


// CONSTANTS NEEDED FOR PERFORMANCE INDICATORS
#define PARTES_DIVISION_PARETO 10    // number of spheres to divide the Pareto front


// function to return the input string, char*, but without the last character

char * toString(int value) {
    static char buffer [5];
    sprintf(buffer, "%d", value);
    return buffer;
}

char * toString(double value) {
    static char buffer [5];
    sprintf(buffer, "%f", value);
    return buffer;
}

char * cutLastChar(char *origen, char *destino) {
    int i, length;

    length = strlen(origen) - 1;

    for (i = 0; i < length; i++) {

        if (origen[i] != '-')
            destino[i] = origen[i];
        else
            destino[i] = '_';
    }

    destino[i] = '\0';
    return destino;
}

void write(string folder, string name, double *values, int maxIt) {
	FILE *ficheroR;

	string output_file = folder + name;
	if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
		fprintf(stderr, "Error opening file to store performance indicator values\n");
		exit(1);
	}

	fprintf(ficheroR, "%f", values[0]);
	for (int i = 1; i < maxIt; i++) {
		fprintf(ficheroR, ";%f", values[i]);
	}
	fprintf(ficheroR, "\n");
	fclose(ficheroR);
}

void writeBinary(string folder, string name, double ** values, int maxIt) {
	FILE *ficheroR;

	string output_file = folder + name;

	if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
		fprintf(stderr, "Error opening file to store performance indicator values\n");
		exit(1);
	}

	for (int i = 0; i < maxIt; i++) {
		fprintf(ficheroR, "%f", values[i][0]);
		for (int j = 1; j < maxIt; j++) {
			fprintf(ficheroR, ";%f", values[i][j]);
		}
		fprintf(ficheroR, "\n");
	}
	fclose(ficheroR);
}

int main(int argc, char *argv[]) {

    // el programa espera un directorio base y dos nombres de algoritmo:: [MOEAD, NSGAII, SPEA2]
    if (argc != 4) {
        fprintf(stderr, "Error while running the application. Checking parameters ");
        fprintf(stderr, "Correct syntaxes is:\n"
                "\tmo_test first_folder/ second_folder/ ");
        exit(1);
    }

    string first_root = argv[1];
    string second_root = argv[2];
    string output_folder = argv[3];

    int maxIt = 3;

    //Unary
    double cardinality_values [maxIt];
    double s_values [maxIt];
    double m1_values [maxIt];
    double m2_values [maxIt];
    double m3_values [maxIt];
    double er_values [maxIt];

    //Binary
    double *ie_values_m [maxIt];
    double *ie_values_a [maxIt];
	double *c_values [maxIt];

	MOSolSet** primeros = new MOSolSet*[maxIt];
	MOSolSet** segundos = new MOSolSet*[maxIt];

	unsigned int dimension = 2;
	MOSolSet* pseudoptimal = new MOSolSet(dimension);

	Point* nadir = new Point(dimension);

	//Read values
	FILE *fichero;

	for (int i = 0; i < maxIt; i++) {
		primeros[i] = new MOSolSet(dimension);
		string firstFile = first_root + "obj" + toString(i) + ".csv";
		if ((fichero = fopen(firstFile.c_str(), "r")) == NULL) {
			fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n",
					firstFile.c_str());
			exit(1);
		}
		primeros[i]->read(fichero);
		pseudoptimal->read(fichero);
		fclose(fichero);

		Point* nadir_first = primeros[i]->getNadirPoint();

		nadir->setNadir(*nadir_first);
		delete nadir_first;

		segundos[i] = new MOSolSet(dimension);
		string secondFile = second_root + "obj" + toString(i) + ".csv";
		if ((fichero = fopen(secondFile.c_str(), "r")) == NULL) {
			fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n",
					secondFile.c_str());
			exit(1);
		}
		segundos[i]->read(fichero);
		pseudoptimal->read(fichero);
		fclose(fichero);

		Point* nadir_second = segundos[i]->getNadirPoint();
		nadir->setNadir(*nadir_second);
		delete nadir_second;

		//Initialize matrixes
		ie_values_m[i] = new double [maxIt];
		ie_values_a[i] = new double [maxIt];
		c_values[i] = new double [maxIt];
	}

    for (int i = 0; i < maxIt; i++) {
    	for (int j = 0; j < maxIt; j++) {
    		ie_values_m[i][j] = primeros[i]->
    				metricaBinariaEpsilon(*segundos[j], false, true);
    		ie_values_a[i][j] = primeros[i]->
    		    				metricaBinariaEpsilon(*segundos[j], true, true);
    		c_values[i][j] = primeros[i]->metricaC(*segundos[j]);
    	}

    	cardinality_values[i] = primeros[i]->size();
    	s_values[i] = primeros[i]->metricaS(*nadir);
    	m1_values[i] = primeros[i]->metricaM1(*pseudoptimal);
    	m3_values[i] = primeros[i]->metricaM3();
    	double sigma = m3_values[i] / PARTES_DIVISION_PARETO;
    	m2_values[i] = primeros[i]->metricaM2(sigma);
    	er_values[i] = primeros[i]->metricaER(*pseudoptimal);
    }

    // Write files
    // S metric
    write(output_folder, "cardinality_values.csv", cardinality_values, maxIt);
    write(output_folder, "s_values.csv", s_values, maxIt);
    write(output_folder, "m1_values.csv", m1_values, maxIt);
    write(output_folder, "m2_values.csv", m2_values, maxIt);
    write(output_folder, "m3_values.csv", m3_values, maxIt);
    write(output_folder, "er_values.csv", er_values, maxIt);

    writeBinary(output_folder, "ie_values_m.csv", ie_values_m, maxIt);
    writeBinary(output_folder, "ie_values_a.csv", ie_values_a, maxIt);
    writeBinary(output_folder, "c_values.csv", c_values, maxIt);

    // Free dynamic memory
    delete pseudoptimal;

    delete[] primeros;
    delete[] segundos;

    fprintf(stdout, "Everything OK!");

    exit(0);

}
