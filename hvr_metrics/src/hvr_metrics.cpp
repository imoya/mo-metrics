//============================================================================
// Name        : hvr_metrics.cpp
// Author      : Ignacio Moya
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "MOSolSet.h"
#include "list.h"


// CONSTANTS NEEDED FOR PERFORMANCE INDICATORS
#define PARTES_DIVISION_PARETO 10    // number of spheres to divide the Pareto front


// function to return the input string, char*, but without the last character

char * toString(int value) {
    static char buffer [5];
    sprintf(buffer, "%d", value);
    return buffer;
}

char * toString(double value) {
    static char buffer [5];
    sprintf(buffer, "%f", value);
    return buffer;
}

char * cutLastChar(char *origen, char *destino) {
    int i, length;

    length = strlen(origen) - 1;

    for (i = 0; i < length; i++) {

        if (origen[i] != '-')
            destino[i] = origen[i];
        else
            destino[i] = '_';
    }

    destino[i] = '\0';
    return destino;
}

int main(int argc, char *argv[]) {

    unsigned int dimension = 2;
    MOSolSet conjunto(dimension);

    double sigma;

    FILE *ficheroR, *fichero;

    // el programa espera un directorio base y dos nombres de algoritmo:: [MOEAD, NSGAII, SPEA2]
    if (argc != 5) {
        fprintf(stderr, "Error while running the application. Checking parameters ");
        fprintf(stderr, "Correct syntaxes is:\n"
                "\thvr_metrics jmetalroot_folder {algorithm: [MOEAD, NSGAII, SPEA2]} max_obj1 max_obj2\n");
        exit(1);
    }

    string based_root = argv[1];

    string algo = argv[2];

    Point origin(atof(argv[3]), atof(argv[4]));

    fprintf(stdout, "Initialized origin: %f %f \n",
            origin.get(0),origin.get(1));

    int maxIt = 20;

    int numPuntos;

    int cardinality_values[maxIt];
    double s_values [maxIt];
    double m2_values [maxIt];
    double m3_values [maxIt];
    //int numPuntos;

    for (int i = 0; i < maxIt; i++) {
        //Loads first set
        conjunto.vacia();
        string firstFile = based_root + "/" + algo + "/MWomABMProblem/FUN" + toString(i) + "_front.tsv";
        if ((fichero = fopen(firstFile.c_str(), "r")) == NULL) {
            fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", firstFile.c_str());
            exit(1);
        }
        numPuntos = conjunto.read(fichero);
        fclose(fichero);

        cardinality_values[i] = numPuntos;

        float s = conjunto.metricaS(origin);

        fprintf(stdout, "METRICA S: %f\n", s);
        s_values[i] = s;

        m3_values[i]= conjunto.metricaM3();

        // metrica M2
        // el radio sigma está calculado a partir del resultado de la métrica M3 (extensión Pareto)
        // y del número de partes o 'bolas' en las que queremos dividir el pareto
        sigma = m3_values[i] / PARTES_DIVISION_PARETO;
        m2_values[i]= conjunto.metricaM2(sigma);

        //        for (int j = 0; j < maxIt; j++) {
        //            //Loads second set
        //            conjuntoSegundo.vacia();
        //            string secondFile = based_root + "/" + second_alg + "/MWomABMProblem/FUN" + toString(j) + "_front.tsv";
        //            if ((fichero = fopen(secondFile.c_str(), "r")) == NULL) {
        //                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", secondFile.c_str());
        //                exit(1);
        //            }
        //            conjuntoSegundo.read(fichero);
        //            fclose(fichero);
        //
        //            c_values[i][j] = conjunto.metricaC(conjuntoSegundo);
        //            m1_values[i][j] = conjunto.metricaM1(conjuntoSegundo);
        //            //Ie computes with additive = false
        //            ie_values[i][j] = conjunto.metricaBinariaEpsilon(conjuntoSegundo, false, true);
        //
        //            //cout << "i: " << i << " j: " << j << endl;
        //        }
    }

    // Write files
    // S metric
    string output_file = based_root + "/" + algo + "_s.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    fprintf(ficheroR, "%f", s_values[0]);
    for (int i = 1; i < maxIt; i++) {
        fprintf(ficheroR, ";%f", s_values[i]);
    }
    fprintf(ficheroR, "\n");
    fclose(ficheroR);

    //M2
    output_file = based_root + "/" + algo + "_m2.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    fprintf(ficheroR, "%f", m2_values[0]);
    for (int i = 1; i < maxIt; i++) {
        fprintf(ficheroR, ";%f", m2_values[i]);
    }
    fprintf(ficheroR, "\n");
    fclose(ficheroR);

    //M3
    output_file = based_root + "/" + algo + "_m3.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    fprintf(ficheroR, "%f", m3_values[0]);
    for (int i = 1; i < maxIt; i++) {
        fprintf(ficheroR, ";%f", m3_values[i]);
    }
    fprintf(ficheroR, "\n");
    fclose(ficheroR);

    //Cardinality
    output_file = based_root + "/" + algo + "_cardinality.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    fprintf(ficheroR, "%u", cardinality_values[0]);
    for (int i = 1; i < maxIt; i++) {
        fprintf(ficheroR, ";%u", cardinality_values[i]);
    }
    fprintf(ficheroR, "\n");
    fclose(ficheroR);

    exit(0);

}
