//============================================================================
// Name        : mo_metrics.cpp
// Author      : Ignacio Moya
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "MOSolSet.h"
#include "list.h"



// CONSTANTS NEEDED FOR PERFORMANCE INDICATORS
#define PARTES_DIVISION_PARETO 10    // number of spheres to divide the Pareto front


// function to return the input string, char*, but without the last character

char * cutLastChar(char *origen, char *destino) {
    int i, length;

    length = strlen(origen) - 1;

    for (i = 0; i < length; i++) {

        if (origen[i] != '-')
            destino[i] = origen[i];
        else
            destino[i] = '_';
    }

    destino[i] = '\0';
    return destino;
}

int main(int argc, char *argv[]) {
    unsigned int dimension = 2;
    MOSolSet conjunto(dimension);
    MOSolSet conjuntoSegundo(dimension);
    float S, m2, m3, m1, epsilon, sigma, C[2];
    int numPuntos;

    // 0 to merge paretos, 1 to compare two with binary indicators, 2 for unary metrics, 3 for ER,
    // 4 for reference point indicators for interactive algorithms
    // 5 for performance indicators when having a reference point. this is for interactive algorithms
    int option = -1;

    bool additive;
    FILE *ficheroR, *fichero;

    // help for the software using --help
    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        fprintf(stdout, "MultiObjective PERFORMANCE INDICATORS v.2 (July 2012)\n");
        fprintf(stdout, "-----------------------------\n");
        fprintf(stdout, "Application for calculating performance indicators in MO problems. Also to merge Pareto fronts from text files.\n");
        fprintf(stdout, "It is useful when we have different Pareto text files with the objective values returning by MO algorithms\n");
        fprintf(stdout, "IMPORTANT: In this version, some indicators just work with bi-objective problems. They were not tested with > 2 objs.\n");

        fprintf(stdout, "We have five possible running options: \n(1) calculate unary indicators for just one Pareto text file\n"
                "(2)  merge more than one Pareto front in just one front\n"
                "(3) calculate binary indicators for two Pareto fronts: M1*, C and Iepsilon\n"
                "(4) Error Ratio (ER) indicator between two Pareto fronts (the second one is the reference Pareto front)\n"
                "(5) Performance indicators when having a reference point. We measure weighted Euclidean distance and coverage to the reference point\n\n");

        fprintf(stdout, "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        fprintf(stdout, "SINTAXIS: mo_metrics [--unir-paretos | --comparar-paretos | --ER | --interactive_indicators | --help] "
                "paretoFile1 paretoFile2 ... paretoFileN outputFile {[maxObj1 maxObj2] | [refPoint1 refPoint2 lowerBound1 upperBound1 lowerBound2 upperBound2]}\n");
        fprintf(stdout, "--unir-paretos :  It receives more than one Pareto file and merges them in just one output file by removing the dominated solutions.\n");
        fprintf(stdout, "--comparar-paretos :  It gets two Pareto files, comparing them by calculating binary indicators M1*, C and ER.\n");
        fprintf(stdout, "--ER :  It gets two Pareto fronts being the second one the optimal Pareto front. It returns Error Ratio in an output file and in console.\n");
        fprintf(stdout, "--interactive_indicators :  Returns distance and coverage of one Pareto front to a reference point.\n");
        fprintf(stdout, "No option :  If we do not specify anything, it calculates performance indicators M2*, M3* and S from the input Pareto front.\n");
        fprintf(stdout, "maxObj1 & maxObj2 : Maximum values for objectives 1 and 2.\n");
        fprintf(stdout, "lowerBound1, lowerBound2, upperBound1 and upperBound2: practical bounds for calculating distances.\n");
        fprintf(stdout, "--help :  This little help.\n\n");
        fprintf(stdout, "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        fprintf(stdout, "Manuel Chica Serrano. manuel.chica.serrano@gmail.es \n");
        exit(1);
    }

    // queremos unir dos paretos
    if (argc >= 5 && strcmp(argv[1], "--unir-paretos") == 0) {
        option = 0;
    }

    // queremos comparar dos ficheros de paretos con métricas M1* y C
    if (argc == 5 && strcmp(argv[1], "--comparar-paretos") == 0) {
        option = 1;
    }

    // queremos generar métricas de análisis de un único pareto
    if (argc == 5 && strcmp(argv[1], "--comparar-paretos") != 0 && strcmp(argv[1], "--unir-paretos") != 0) {
        option = 2;
    }

    // queremos comparar dos ficheros de paretos con métricas M1* y C
    if (argc == 5 && strcmp(argv[1], "--ER") == 0) {
        option = 3;
    }

    // we want to calculate distances and areas to a reference point
    if (argc == 10 && strcmp(argv[1], "--interactive_indicators") == 0) {
        option = 4;
    }

    // los parámetros están mal para las 3 posibles variantes de la utilidad
    if (option == -1) {
        fprintf(stderr, "Error while running the application. Checking parameters ");
        fprintf(stderr, "Correct syntaxes is:\n"
                "\tmo_metrics [--unir-paretos | --comparar-paretos | --ER | --interactive_indicators] <Paretofiles> <outputFile> "
                "{[maxObj1 maxObj2] | [refPoint1 refPoint2 lowerBound1 upperBound1 lowerBound2 upperBound2]}.\n");
        fprintf(stderr, "Type mo_metrics --help for more information. \n");
        exit(1);
    }


    switch (option) {

        case 0: // MERGE PARETO FRONTS
        {
            // load non dominated solutions
            conjunto.vacia();

            // leemos desde los parámetros del ejecutable los ficheros con los paretos de entrada
            for (int i = 2; i < (argc - 1); i++) {

                if ((fichero = fopen(argv[i], "r")) == NULL) {
                    fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[i]);
                    exit(1);
                }

                printf("Pareto front %s read!\n", argv[i]);

                conjunto.read(fichero);

                fclose(fichero);
            }

            // store in an output file
            if ((ficheroR = fopen(argv[(argc - 1)], "w")) == NULL) {
                fprintf(stderr, "Error opening file %s to store performance indicator values\n", argv[(argc - 1)]);
                exit(1);
            }

            conjunto.write(ficheroR);
            fclose(ficheroR);

            printf("Output Pareto front successfully stored in file %s\n", argv[(argc - 1)]);
        }
            break;

        case 1: // INDICATORS TO COMPARE 2 PARETO FRONTS
        {
            // we read both Pareto fronts from files and create the set of optimal solutions

            conjunto.vacia();
            conjuntoSegundo.vacia();

            if ((fichero = fopen(argv[2], "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[2]);
                exit(1);
            }

            conjunto.read(fichero);
            fclose(fichero);

            if ((fichero = fopen(argv[3], "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[3]);
                exit(1);
            }


            conjuntoSegundo.read(fichero);

            fclose(fichero);

            // calculate C performance indicator for the two optimal sets
            C[0] = conjunto.metricaC(conjuntoSegundo);
            //C[1] = conjuntoSegundo.metricaC(conjunto);

            // calculate M1 performance indicator for the two optimal sets
            m1 = conjunto.metricaM1(conjuntoSegundo);

            // calculate Iepsilon performance indicator for the two optimal sets using additive and always minimizing
            additive = false;
            epsilon = conjunto.metricaBinariaEpsilon(conjuntoSegundo, additive, true);

            // end to calculate using ParadisEO

            // open the output file
            if ((ficheroR = fopen(argv[(argc - 1)], "w")) == NULL) {
                fprintf(stderr, "Error opening file to store performance indicator values\n");
                exit(1);
            }

            // print the results in the screen and file shwoing the results
            fprintf(ficheroR, "METRICA C (Pareto 1 sobre 2): %f\n", C[0]);
            printf("METRICA C: %f\n", C[0]);

            fprintf(ficheroR, "METRICA M1: %f\n", m1);
            printf("METRICA M1: %f\n", m1);

            if (additive) {
                fprintf(ficheroR, "METRICA epsilon(%s): %f\n", "additive", epsilon);
                printf("METRICA epsilon(%s): %f\n", "additive", epsilon);
            } else {
                fprintf(ficheroR, "METRICA epsilon(%s): %f\n", "multipl.", epsilon);
                printf("METRICA epsilon(%s): %f\n", "multipl.", epsilon);
            }

            fclose(ficheroR);
        }
            break;

        case 2: // UNARY PERFORMANCE INDICATORS FOR ONE PARETO FRONT
        {
            if ((ficheroR = fopen(argv[2], "w")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[2]);
                exit(1);
            }

            if ((fichero = fopen(argv[1], "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[1]);
                exit(1);
            }

            Point origin(atof(argv[3]), atof(argv[4]));

            conjunto.vacia();
            numPuntos = conjunto.read(fichero);
            fclose(fichero);


            // metrica S tomando como origen el punto (0,0)
            /*aux = conjunto1.metricaS();

                    fprintf(ficheroR, "METRICA S: %f\n", aux);
                    printf("METRICA S: %f\n", aux);
             */

            // metrica S con origen el máximo posible
            S = conjunto.metricaS(origin);

            // metrica M3
            m3 = conjunto.metricaM3();

            // metrica M2
            // el radio sigma está calculado a partir del resultado de la métrica M3 (extensión Pareto)
            // y del número de partes o 'bolas' en las que queremos dividir el pareto
            sigma = m3 / PARTES_DIVISION_PARETO;
            m2 = conjunto.metricaM2(sigma);


            // imprimos por pantalla y en archivo los resultados de las métricas
            fprintf(ficheroR, "# SOLUCIONES: %d\n", numPuntos);
            printf("#SOLUCIONES: %d\n", numPuntos);
            fprintf(ficheroR, "# SOLUCIONES DISTINTAS: %d\n", conjunto.size());
            printf("#SOLUCIONES DISTINTAS: %u\n", conjunto.size());
            fprintf(ficheroR, "METRICA S: %f\n", S);
            printf("METRICA S (origen el maximo): %f\n", S);
            fprintf(ficheroR, "METRICA M2: %f\n", m2);
            printf("METRICA M2: %f\n", m2);
            fprintf(ficheroR, "METRICA M3: %f\n", m3);
            printf("METRICA M3: %f\n", m3);

            fclose(ficheroR);
        }
            break;

        case 3: // ERROR RATIO
        {
            // we read both Pareto front files

            conjunto.vacia();
            conjuntoSegundo.vacia();

            if ((fichero = fopen(argv[2], "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[2]);
                exit(1);
            }

            conjunto.read(fichero);
            fclose(fichero);

            if ((fichero = fopen(argv[3], "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[3]);
                exit(1);
            }

            conjuntoSegundo.read(fichero);
            fclose(fichero);

            // calculamos la metrica ER entre los dos conjuntos
            C[0] = conjunto.metricaER(conjuntoSegundo);

            // abrimos el fichero de salida
            if ((ficheroR = fopen(argv[(argc - 1)], "w")) == NULL) {
                fprintf(stderr, "Error opening file to store performance indicator values\n");
                exit(1);
            }

            // imprimos por pantalla y en archivo los resultados de las métricas
            fprintf(ficheroR, "METRICA ER: %f\n", C[0]);
            printf("METRICA ER: %f\n", C[0]);

            fclose(ficheroR);
        }

            break;

        case 4: // REFERENCE POINT BASED PERFORMANCE INDICATORS FOR ONE PARETO FRONT
        {
            // read Pareto front file

            conjunto.vacia();

            if ((fichero = fopen(argv[2], "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", argv[2]);
                exit(1);
            }

            conjunto.read(fichero);
            fclose(fichero);

            // read ref point
            Point refPoint(atof(argv[4]), atof(argv[5]));

            float lowerBounds[2];
            float upperBounds[2];

            lowerBounds[0] = atof(argv[6]);
            upperBounds[0] = atof(argv[7]);

            lowerBounds[1] = atof(argv[8]);
            upperBounds[1] = atof(argv[9]);

            float avgDist = -1.;
            float stdDist = -1.;

            // calculate average and std of the weighted Euclidean distances
            conjunto.metricaDistance2RefPoint(refPoint, lowerBounds, upperBounds, avgDist, stdDist);

            // calculate coverage of the solutions in the 1st quadrant from reference point
            float coverageValue = conjunto.metricaQuadrantAreas2RefPoint(refPoint, lowerBounds, upperBounds);

            // open output file
            if ((ficheroR = fopen(argv[3], "w")) == NULL) {
                fprintf(stderr, "Error opening file to store performance indicator values\n");
                exit(1);
            }

            // print in the screen and console the results of the performance indicators
            fprintf(ficheroR, "METRICA AVGDIST: %f\n", avgDist);
            fprintf(ficheroR, "METRICA STDDIST: %f\n", stdDist);
            printf("METRICA AVGDIST: %f\n", avgDist);
            printf("METRICA STDDIST: %f\n", stdDist);

            fprintf(ficheroR, "METRICA COV: %f\n", coverageValue);
            printf("METRICA COV: %f\n", coverageValue);

            fclose(ficheroR);
        }
            break;

    }

    return 0;

}

