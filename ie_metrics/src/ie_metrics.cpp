//============================================================================
// Name        : ie_metrics.cpp
// Author      : Ignacio Moya
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "MOSolSet.h"
#include "list.h"



// CONSTANTS NEEDED FOR PERFORMANCE INDICATORS
#define PARTES_DIVISION_PARETO 10    // number of spheres to divide the Pareto front


// function to return the input string, char*, but without the last character

char * toString(int value) {
    static char buffer [5];
    sprintf(buffer,"%d",value);
    return buffer;
}

char * cutLastChar(char *origen, char *destino) {
    int i, length;

    length = strlen(origen) - 1;

    for (i = 0; i < length; i++) {

        if (origen[i] != '-')
            destino[i] = origen[i];
        else
            destino[i] = '_';
    }

    destino[i] = '\0';
    return destino;
}

int main(int argc, char *argv[]) {

    unsigned int dimension = 2;
    MOSolSet conjunto(dimension);
    MOSolSet conjuntoSegundo(dimension);

    FILE *ficheroR, *fichero;

    // el programa espera un directorio base y dos nombres de algoritmo:: [MOEAD, NSGAII, SPEA2]
    if (argc != 4) {
        fprintf(stderr, "Error while running the application. Checking parameters ");
        fprintf(stderr, "Correct syntaxes is:\n"
                "\tmo_metrics jmetalroot_folder two_algorithms: [MOEAD, NSGAII, SPEA2]\n");
        exit(1);
    }

    string based_root = argv[1];

    string firt_alg = argv[2];

    string second_alg = argv[3];


    int maxIt = 20;

    double ie_values [maxIt][maxIt];
    double c_values [maxIt][maxIt];
    double m1_values [maxIt][maxIt];

    for (int i = 0; i < maxIt; i++) {
        //Loads first set
        conjunto.vacia();
        string firstFile = based_root + "/" + firt_alg + "/MWomABMProblem/FUN" + toString(i) + "_front.tsv";
        if ((fichero = fopen(firstFile.c_str(), "r")) == NULL) {
            fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", firstFile.c_str());
            exit(1);
        }
        conjunto.read(fichero);
        fclose(fichero);

        for (int j = 0; j < maxIt; j++) {
            //Loads second set
            conjuntoSegundo.vacia();
            string secondFile = based_root + "/" + second_alg + "/MWomABMProblem/FUN" + toString(j) + "_front.tsv";
            if ((fichero = fopen(secondFile.c_str(), "r")) == NULL) {
                fprintf(stderr, "Error opening file %s to read objective values of the Pareto front\n", secondFile.c_str());
                exit(1);
            }
            conjuntoSegundo.read(fichero);
            fclose(fichero);

            c_values[i][j] = conjunto.metricaC(conjuntoSegundo);
            m1_values[i][j] = conjunto.metricaM1(conjuntoSegundo);
            //Ie computes with additive = false
            ie_values[i][j] = conjunto.metricaBinariaEpsilon(conjuntoSegundo, false, true);

            //cout << "i: " << i << " j: " << j << endl;
        }
    }

    // Write files
    // C metric
    string output_file = based_root + "/" + firt_alg + "_vs_" + second_alg + "_c.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    for (int i = 0; i < maxIt; i++) {
        fprintf(ficheroR, "%f", c_values[i][0]);
        for (int j = 1; j < maxIt; j++) {
            fprintf(ficheroR, ";%f", c_values[i][j]);
        }
        fprintf(ficheroR, "\n");
    }
    fclose(ficheroR);


    //M1 metric
    output_file = based_root + "/" + firt_alg + "_vs_" + second_alg + "_m1.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    for (int i = 0; i < maxIt; i++) {
        fprintf(ficheroR, "%f", m1_values[i][0]);
        for (int j = 1; j < maxIt; j++) {
            fprintf(ficheroR, ";%f", m1_values[i][j]);
        }
        fprintf(ficheroR, "\n");
    }
    fclose(ficheroR);

    //M1 metric
    output_file = based_root + "/" + firt_alg + "_vs_" + second_alg + "_ie.csv";
    if ((ficheroR = fopen(output_file.c_str(), "w")) == NULL) {
        fprintf(stderr, "Error opening file to store performance indicator values\n");
        exit(1);
    }

    for (int i = 0; i < maxIt; i++) {
        fprintf(ficheroR, "%f", ie_values[i][0]);
        for (int j = 1; j < maxIt; j++) {
            fprintf(ficheroR, ";%f", ie_values[i][j]);
        }
        fprintf(ficheroR, "\n");
    }
    fclose(ficheroR);

    printf("Done! Everything Ok! \n");

    exit(0);

}
